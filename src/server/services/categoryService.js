'use strict';

var CategoryService = {};

CategoryService.getCategories = function getCategories(connection) {
    var sql = "SELECT id, name FROM category";
    return connection.query(sql);
};

CategoryService.getCategory = function getCategory(connection, id) {
    var sql = "SELECT id, name FROM category WHERE id = :id";

    return connection.query(sql, {id : id});
}

module.exports = CategoryService;